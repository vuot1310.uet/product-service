/* eslint-disable no-underscore-dangle */
/* eslint-disable consistent-return */
import { type Request, type Response, type NextFunction } from 'express';
import httpStatus from 'http-status';
import { type JwtPayload } from 'jsonwebtoken';
import APIError from '../../../helpers/APIError';
import UserModel from './user.model';
import errorcode from '../../../constants/errorCode';
import authToken from '../../../helpers/authToken';
import { type ICustomJwtPayload } from '../../../constants/interfaces';
import redisClient from '../../../helpers/redisClient';

async function register(req: Request, res: Response, next: NextFunction) {
  try {
    const { username, password } = req.body;
    const isExists = await UserModel.findOne({
      username,
    });

    if (isExists) {
      next(new APIError('Tai khoan da ton tai', httpStatus.BAD_REQUEST, errorcode.USER_NOT_AVAIBLE));
      return;
    }
    const userCreated = await UserModel.create({
      username,
      password,
    });

    return res.status(httpStatus.OK).json(userCreated);
  } catch (err) {
    next(err);
  }
}

async function login(req: Request, res: Response, next: NextFunction) {
  try {
    const { username, password } = req.body;
    const user = await UserModel.findOne({ username });
    if (!user) {
      throw new APIError('Tai khoan khong ton tai', httpStatus.NOT_FOUND, errorcode.OBJECT_NOT_FOUND);
    }

    const isValid = await user.isCheckPassword(password);
    if (!isValid) {
      throw new APIError('Tai khoan hoac mat khau khong chinh xac', httpStatus.BAD_REQUEST, errorcode.AUTHENICATE_FAIL);
    }

    const accessToken = await authToken.signAccessToken(user._id);
    // eslint-disable-next-line @typescript-eslint/no-shadow
    const refreshToken = await authToken.signRefreshToken(user._id);
    return res.json({ accessToken, refreshToken });
  } catch (err) {
    next(err);
  }
}

async function logout(req: Request, res: Response, next: NextFunction) {
  try {
    // eslint-disable-next-line @typescript-eslint/no-shadow
    const { refreshToken } = req.body;
    if (!refreshToken) {
      throw new APIError('No refresh token', httpStatus.BAD_REQUEST, errorcode.EXCEPTION);
    }
    const { userId } = (await authToken.verifyRefreshToken(refreshToken)) as ICustomJwtPayload;
    const deleteToken = await redisClient.del(userId.toString());
    if (!deleteToken) {
      next(new APIError('', httpStatus.INTERNAL_SERVER_ERROR));
      return;
    }
    return res.status(httpStatus.OK).json({
      message: 'Logout',
    });
  } catch (err) {
    next(err);
  }
}
async function getListUsers(req: Request, res: Response, next: NextFunction) {
  try {
    return res.json([
      {
        email: 'huuvuot2001@gmail.com',
      },
      {
        email: 'vuotnh2001@gmail.com',
      },
    ]);
  } catch (err) {
    next(err);
  }
}

async function refreshToken(req: Request, res: Response, next: NextFunction) {
  try {
    // eslint-disable-next-line @typescript-eslint/no-shadow
    const { refreshToken } = req.body;
    if (!refreshToken) throw new APIError('No refresh token', httpStatus.BAD_REQUEST);
    const payload: JwtPayload = await authToken.verifyRefreshToken(refreshToken);
    const accessToken = await authToken.signAccessToken(payload.userId);
    const refToken = await authToken.signAccessToken(payload.userId);
    return res.json({
      accessToken,
      refreshToken: refToken,
    });
  } catch (err) {
    next(err);
  }
}

const UserController = {
  register,
  login,
  getListUsers,
  refreshToken,
  logout,
};

export default UserController;
