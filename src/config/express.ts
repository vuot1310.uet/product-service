// eslint-disable-next-line import/no-extraneous-dependencies
import client from 'prom-client';
import express, { type Response, type Request, type NextFunction } from 'express';
import * as dotenv from 'dotenv';
import { type ValidationError as JoiValidationError } from 'joi';
import { ValidationError } from 'express-validation';
import httpStatus from 'http-status';
import router from '../server/index.route';
import errorcode from '../constants/errorCode';
import APIError from '../helpers/APIError';

dotenv.config();

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// setup monitoring
const register = new client.Registry();
// Add a default label which is added to all metrics
register.setDefaultLabels({
  app: 'product-service',
});

// Enable the collection of default metrics
client.collectDefaultMetrics({ register });

// Create a custom histogram metric
const httpRequestTimer = new client.Histogram({
  name: 'http_request_duration_ms',
  help: 'Duration of HTTP requests in seconds',
  labelNames: ['method', 'route', 'code'],
  buckets: [0.1, 5, 15, 50, 100, 200, 300, 400, 500], // 0.1 to 10 seconds
});
// Register the histogram
register.registerMetric(httpRequestTimer);

// handle metrix middlware
app.use((req: Request, res: Response, next: NextFunction) => {
  const end = httpRequestTimer.startTimer();
  res.on('finish', () => {
    end({
      method: req.method,
      route: new URL(req.url, `http://${req.hostname}`).pathname,
      code: `${res.statusCode}`,
    });
  });
  next();
});

app.use('/api', router);

// export metrics
app.get('/metrics', async (req: Request, res: Response) => {
  res.setHeader('Content-Type', register.contentType);
  res.end(await register.metrics());
});

app.use((err: Error | ValidationError, req: Request, res: Response, next: NextFunction) => {
  if (err instanceof ValidationError) {
    const detailsBody: JoiValidationError[] | undefined = err.details.body;
    const detailsQuery: JoiValidationError[] | undefined = err.details.query;
    if (detailsBody) {
      const errorMessageArr: string[] = detailsBody.map((thizBody) => thizBody.message);
      const error = new APIError(errorMessageArr.join(', '), httpStatus.BAD_REQUEST, errorcode.VALIDATE);
      next(error);
      return;
    }
    if (detailsQuery) {
      const errorMessageArr: string[] = detailsQuery.map((thizBody) => thizBody.message);
      const error = new APIError(errorMessageArr.join(', '), httpStatus.BAD_REQUEST, errorcode.VALIDATE);
      next(error);
      return;
    }
    const error = new APIError(err.message, httpStatus.BAD_REQUEST, errorcode.VALIDATE);
    next(error);
    return;
  }
  if (!(err instanceof APIError)) {
    const apiError = new APIError(err.message, httpStatus.INTERNAL_SERVER_ERROR, errorcode.EXCEPTION, '', err.stack);
    next(apiError);
    return;
  }
  // the rest is 404
  next(err);
});

// catch 404 and forward to error handler
// must have to use middleware below
app.use((req, res, next) => {
  const err = new APIError('NOT FOUND', httpStatus.NOT_FOUND, errorcode.OBJECT_NOT_FOUND);
  return next(err); // next to err
});

app.use((err: APIError, req: Request, res: Response, next: NextFunction) => {
  return res.json({
    status: err.status || 500,
    message: err.message,
  });
});
export default app;
