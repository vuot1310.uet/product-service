import { Router } from 'express';
import UserRouter from './api/user/user.route';
import VendorRouter from './api/vendor/vendor.route';

const router = Router();

router.route('/').get((req, res) => {
  res.send('Hello World!');
});

router.route('/health-check').get((req, res) => {
  res.send('Healthy!');
});

router.use('/auth', UserRouter);
router.use('/vendor', VendorRouter);

export default router;
