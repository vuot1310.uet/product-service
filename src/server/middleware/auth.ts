import { type Request, type Response, type NextFunction } from 'express';
import httpStatus from 'http-status';
import jsonwebtoken from 'jsonwebtoken';
import errorcode from '../../constants/errorCode';
import { type IRequestWithMoreInfo } from '../../constants/interfaces';
import APIError from '../../helpers/APIError';

const isAuth = (req: Request, res: Response, next: NextFunction) => {
  if (!req.headers.authorization) {
    next(new APIError('Vui long dang nhap', httpStatus.UNAUTHORIZED, errorcode.AUTHENICATE_FAIL));
    return;
  }

  const authHeader = req.headers.authorization;
  const bearerToken = authHeader.split(' ')[1];
  jsonwebtoken.verify(bearerToken, process.env.ACCESS_TOKEN_SECRET, (err, payload) => {
    if (err) {
      if (err.name === 'TokenExpiredError') {
        next(new APIError('Token expired', httpStatus.UNAUTHORIZED, errorcode.AUTHENICATE_FAIL));
        return;
      }
      next(new APIError('Vui long dang nhap', httpStatus.UNAUTHORIZED, errorcode.AUTHENICATE_FAIL));
      return;
    }
    (req as IRequestWithMoreInfo).payload = payload;
    next();
  });
};

const AuthMiddleware = {
  isAuth,
};

export default AuthMiddleware;
