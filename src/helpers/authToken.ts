import { type NextFunction, type Request, type Response } from 'express';
import jsonwebtoken from 'jsonwebtoken';
import type mongoose from 'mongoose';
import httpStatus from 'http-status';
import APIError from './APIError';
import errorcode from '../constants/errorCode';
import { type ICustomJwtPayload, type IRequestWithMoreInfo } from '../constants/interfaces';
import redisClient from './redisClient';

const signAccessToken = async (userId: mongoose.Types.ObjectId) => {
  return new Promise((resolve, reject) => {
    const payload = {
      userId,
    };
    const secret = process.env.ACCESS_TOKEN_SECRET;
    const options = {
      expiresIn: '1h', // 10m 10s
    };
    jsonwebtoken.sign(payload, secret, options, (err, token) => {
      if (err) reject(err);
      resolve(token);
    });
  });
};

const signRefreshToken = async (userId: mongoose.Types.ObjectId) => {
  return new Promise((resolve, reject) => {
    const payload = {
      userId,
    };
    const secret = process.env.REFRESH_TOKEN_SECRET;
    const options = {
      expiresIn: '1w', // 10m 10s
    };
    jsonwebtoken.sign(payload, secret, options, (err, token) => {
      if (err) reject(err);
      redisClient.set(userId.toString(), token, 7 * 24 * 60 * 60); // expire in 1week
      resolve(token);
    });
  });
};

const verifyAccessToken = (req: Request, res: Response, next: NextFunction) => {
  if (!req.headers.authorization) {
    next(new APIError('Vui long dang nhap', httpStatus.UNAUTHORIZED, errorcode.AUTHENICATE_FAIL));
    return;
  }

  const authHeader = req.headers.authorization;
  const bearerToken = authHeader.split(' ')[1];
  jsonwebtoken.verify(bearerToken, process.env.ACCESS_TOKEN_SECRET, (err, payload) => {
    if (err) {
      if (err.name === 'TokenExpiredError') {
        next(new APIError('Token expired', httpStatus.UNAUTHORIZED, errorcode.AUTHENICATE_FAIL));
        return;
      }
      next(new APIError('Vui long dang nhap', httpStatus.UNAUTHORIZED, errorcode.AUTHENICATE_FAIL));
      return;
    }
    (req as IRequestWithMoreInfo).payload = payload;
    next();
  });
};

const verifyRefreshToken = async (refreshToken: string) => {
  return new Promise((resolve, reject) => {
    jsonwebtoken.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, async (err, payload) => {
      if (err) {
        reject(err);
      }
      const redisRefreshToken = await redisClient.get((payload as ICustomJwtPayload).userId);
      if (!redisRefreshToken) {
        reject(new APIError('Do not find refresh token in redis', httpStatus.INTERNAL_SERVER_ERROR));
      }

      if (redisRefreshToken === refreshToken) {
        resolve(payload);
      }
      reject(new APIError('Unauthorized', httpStatus.UNAUTHORIZED, errorcode.AUTHENICATE_FAIL));
    });
  });
};

const authTokenService = {
  signAccessToken,
  verifyAccessToken,
  signRefreshToken,
  verifyRefreshToken,
};

export default authTokenService;
