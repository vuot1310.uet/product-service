import express from 'express';
import { validate } from 'express-validation';
import VendorController from './vendor.controller';
import VenderValidator from './vendor.validation';

const VendorRouter = express.Router();

VendorRouter.route('/')
  .post(validate(VenderValidator.createVendorValidation), VendorController.createNewVendor)
  .get(VendorController.list);

export default VendorRouter;
