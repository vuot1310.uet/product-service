/* eslint-disable import/no-unresolved */
// test performance with k6.io
import http from 'k6/http';
import { sleep, check } from 'k6';

export const options = {
  scenarios: {
    contacts: {
      executor: 'per-vu-iterations',
      vus: 100,
      iterations: 1000,
      maxDuration: '30m',
    },
  },
};

export default function () {
  const res = http.get('http://localhost:3003/api/vendor');
  check(res, {
    'is status 200': (r) => r.status === 200,
  });
  sleep(1);
}
