FROM node:16-alpine
WORKDIR /app

# Create a group and user
RUN addgroup -S groupauthen && adduser -S userauthen -G groupauthen
RUN chown -R userauthen:groupauthen /app

# Tell docker that all future commands should run as the appuser user
USER userauthen

# copy from host to container and change role to userauthen
COPY --chown=userauthen:groupauthen package.json .

ARG NODE_ENV
RUN if ["$NODE_ENV" = "development"]; \
            then npm install; \
            else npm cache clean --force && npm install --production; \
            fi

COPY --chown=userauthen:groupauthen . .

RUN npm run build


