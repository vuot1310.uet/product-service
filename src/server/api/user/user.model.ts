import mongoose from 'mongoose';
import bcryptjs from 'bcryptjs';

const UserSchema = new mongoose.Schema(
  {
    username: {
      type: String,
      lowercase: true,
      unique: true,
      require: true,
    },
    password: {
      type: String,
      required: true,
    },
  },
  {
    methods: {
      async isCheckPassword(password) {
        try {
          return await bcryptjs.compare(password, this.password);
        } catch (err) {
          throw new Error(err.message);
        }
      },
    },
  },
);

UserSchema.pre('save', async function (next) {
  try {
    const salt = await bcryptjs.genSalt(10);
    const hashPassword = await bcryptjs.hash(this.password, salt);
    this.password = hashPassword;
    next();
  } catch (err) {
    next(err);
  }
});

const UserModel = mongoose.model('User', UserSchema);

export default UserModel;
