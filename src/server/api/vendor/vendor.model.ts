import mongoose, { Document, Schema } from 'mongoose';

interface VendorDoc extends Document {
  name: string;
  owner: any;
  foodType: [string];
  pincode: string;
  address: string;
  phone: string;
  email: string;
  serviceAvailabel: boolean;
  coverImage: string;
  rating: number;
  foods: any;
}

const VendorSchema = new Schema(
  {
    name: { type: String, required: true },
    owner: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'user',
    },
    foodType: { type: [String] },
    address: { type: String, rquired: true },
    email: { type: String, required: true },
    phone: { type: String, required: true },
    serviceAvailabel: { type: Boolean },
    coverImage: { type: [String] },
    rating: { type: Number },
    foods: [
      {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'food',
      },
    ],
  },
  {
    timestamps: true,
  },
);

const Vendor = mongoose.model<VendorDoc>('vendor', VendorSchema);

export default Vendor;
