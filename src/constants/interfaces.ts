import { Request } from 'express';
import { JwtPayload } from 'jsonwebtoken';

export interface IAccessTokenPayload {
  id: string;
  username: string;
}
export interface IRequestWithMoreInfo extends Request {
  user?: IAccessTokenPayload;
  payload?: string | JwtPayload;
}

export interface ICustomJwtPayload extends JwtPayload {
  userId: string;
}
