import { Request, Response, NextFunction } from 'express';
import httpStatus from 'http-status';
import Vendor from './vendor.model';
import APIError from '../../../helpers/APIError';
import redisClient from '../../../helpers/redisClient';

async function createNewVendor(req: Request, res: Response, next: NextFunction) {
  try {
    const { name, foodType, pincode, address, email, phone, serviceAvailabel, coverImage, rating } = req.body;
    const vendorCreated = await Vendor.create({
      name,
      foodType,
      pincode,
      address,
      email,
      phone,
      serviceAvailabel,
      coverImage,
      rating,
    });

    if (!vendorCreated) {
      return next(new APIError('Create new vender failed', httpStatus.INTERNAL_SERVER_ERROR));
    }
    return res.status(httpStatus.OK).json(vendorCreated);
  } catch (err) {
    return next(err);
  }
}

async function list(req: Request, res: Response, next: NextFunction) {
  try {
    const listVendor = await Vendor.find();
    return res.status(httpStatus.OK).json(listVendor);
  } catch (err) {
    return next(err);
  }
}

async function listCache(req: Request, res: Response, next: NextFunction) {
  try {
    const listVendor = await redisClient.getOrSet(
      'listVendor',
      async () => {
        const item = await Vendor.find();
        return item;
      },
      30,
    );
    return res.status(httpStatus.OK).json(listVendor);
  } catch (err) {
    return next(err);
  }
}
const VendorController = {
  createNewVendor,
  list,
  listCache,
};

export default VendorController;
