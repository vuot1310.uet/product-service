import { Joi } from 'express-validation';

const createVendorValidation = {
  body: Joi.object({
    name: Joi.string().required(),
    foodType: Joi.array().items(Joi.string()),
    pincode: Joi.string().required(),
    address: Joi.string().required(),
    email: Joi.string().required(),
    phone: Joi.string().required(),
    serviceAvailabel: Joi.boolean(),
    coverImage: Joi.array().items(Joi.string().allow('', null)),
    rating: Joi.number(),
  }),
};

const VenderValidator = {
  createVendorValidation,
};

export default VenderValidator;
